/* ============================================================ */
/* Настройки */
/* ============================================================ */

// идентификатор элемента для анимирования
var label_id = "label";

// траектория движения
// позиции
// 1 2 3
// 4 5 6
// 7 8 9
var trajectory = [1, 7, 2, 9, 3];

// длительность одного шага анимации (мс)
var duration = 2000;              


// тип функции
// 0 - синусоида
// 1 - косинусоида
var animate_function = 0;

// направление 
// 0 - вверх  (↑)
// 1 - вниз   (↓)
// 2 - вправо (→)
// 3 - влево  (←)
var direction = 2;



/* ============================================================ */
/* jQuery */
/* ============================================================ */

$(document).ready(function() {

    var label = $("#"+label_id);

    /* ============================================================ */
    /* кнопки меню */
    /* ============================================================ */

    var main_menu_buttons = $(".menu-tabs a");
    var control_buttons   = $("#control-buttons");

    var follow_cursor  = false; // следовать за мышью
    var cursor_runaway = false; // убегать от мыши

    main_menu_buttons.on("click", function() {
        main_menu_buttons.removeClass("active");
        $(this).addClass("active");
        $(".text span").removeClass("marked");
    });

    function stop_all() {
        label.css({ display: "block" });        // отображаем элемент
        label.stop(true);                       // останавливаем анимацию и очищаем очередь
        clearInterval(iv);                      // останавливаем цикл в setInterval()
        control_buttons.hide();                 // прячем кнопки управления
        label.draggable({ disabled: true });    // убираем возможность перетаскивать элемент
        follow_cursor  = false;                 // сбрасываем флаг "следовать за мышью""
        cursor_runaway = false;                 // сбрасываем флаг "убегать от мыши""
    }

    $("#menu-item-1").on("click", function() {
        $("#task-1").addClass("marked");
        stop_all();
        animate_trajectory(label, trajectory, duration);
    });

    $("#menu-item-2").on("click", function() {
        $("#task-2").addClass("marked");
        stop_all();
        animate_trajectory(label, trajectory, duration, true);
    });

    $("#menu-item-3").on("click", function() {
        $("#task-3").addClass("marked");
        stop_all();
        animate_trajectory_by_function(label, animate_function, direction);
    });

    $("#menu-item-4").on("click", function() {
        $("#task-4").addClass("marked");
        stop_all();
        animate_trajectory_by_function(label, animate_function, direction, true);
    });

    $("#menu-item-5").on("click", function() {
        $("#task-5").addClass("marked");
        stop_all();
        control_buttons.show();

    });

    $("#menu-item-6").on("click", function() {
        $("#task-6").addClass("marked");
        stop_all();
    });

    $("#menu-item-7").on("click", function() {
        $("#task-7").addClass("marked");
        stop_all();
        label.draggable( {
            disabled: false, 
            containment:"window", 
            drag: function(event, ui) { return !(ui.position.top < 0); } 
        } );
    });

    $("#menu-item-8").on("click", function() {
        $("#task-8").addClass("marked");
        stop_all();
        follow_cursor = true;
    });

    $("#menu-item-9").on("click", function() {
        $("#task-9").addClass("marked");
        stop_all();
        cursor_runaway = true;
    });



    /* ============================================================ */
    /* анимация по траектории */
    /* ============================================================ */

    var box_height = label.outerHeight();   // ширина блока
    var box_width  = label.outerWidth();    // высота блока

    function v_height() { return $(document).height(); } // высота документа
    function v_width()  { return $(document).width(); }  // ширина документа

    function pv_1() { return 0; };                                  // top
    function pv_2() { return v_height() / 2 - box_height / 2; };    // center
    function pv_3() { return v_height() - box_height; };            // bottom

    function ph_1() { return 0; };                                  // left
    function ph_2() { return v_width() / 2 - box_width / 2; };      // center
    function ph_3() { return v_width() - box_width; };              // right

    function animate_to_position(elem, position, params) {
        switch (position) {
            case 1: elem.animate({ top: pv_1(), left: ph_1() }, params); break;
            case 2: elem.animate({ top: pv_1(), left: ph_2() }, params); break;
            case 3: elem.animate({ top: pv_1(), left: ph_3() }, params); break;
            case 4: elem.animate({ top: pv_2(), left: ph_1() }, params); break;
            case 5: elem.animate({ top: pv_2(), left: ph_2() }, params); break;
            case 6: elem.animate({ top: pv_2(), left: ph_3() }, params); break;
            case 7: elem.animate({ top: pv_3(), left: ph_1() }, params); break;
            case 8: elem.animate({ top: pv_3(), left: ph_2() }, params); break;
            case 9: elem.animate({ top: pv_3(), left: ph_3() }, params); break;

            default: // TODO
        }
    }

    function move_to_position(elem, position) {
        switch (position) {
            case 1: elem.css({ top: pv_1(), left: ph_1() }); break;
            case 2: elem.css({ top: pv_1(), left: ph_2() }); break;
            case 3: elem.css({ top: pv_1(), left: ph_3() }); break;
            case 4: elem.css({ top: pv_2(), left: ph_1() }); break;
            case 5: elem.css({ top: pv_2(), left: ph_2() }); break;
            case 6: elem.css({ top: pv_2(), left: ph_3() }); break;
            case 7: elem.css({ top: pv_3(), left: ph_1() }); break;
            case 8: elem.css({ top: pv_3(), left: ph_2() }); break;
            case 9: elem.css({ top: pv_3(), left: ph_3() }); break;

            default: // TODO
        }
    }

    function animate_trajectory(elem, trajectory, dur, back) {

        var params = { duration: dur, easing: "linear" };
        var i;

        // задаем начальное значение
        move_to_position(elem, trajectory[0]);

        // анимиуем траекторию
        for (i = 1; i < trajectory.length; ++i) {
            animate_to_position(elem, trajectory[i], params);
        }

        if (back) {
            // в обратную сторону
            for (i = trajectory.length - 1; i > 0; --i) {
                animate_to_position(elem, trajectory[i], params);
            }
            // последний шаг
            params.complete = function() { animate_trajectory(elem, trajectory, dur, true) };
            animate_to_position(elem, trajectory[0], params);  

        } else {
            // последний шаг
            params.complete = function() { animate_trajectory(elem, trajectory, dur) };
            animate_to_position(elem, trajectory[trajectory.length - 1], params);    
        }
    }



    /* ============================================================ */
    /* движение по синусоиде / косинусоиде */
    /* ============================================================ */

    var iv; // интервал

    function animate_trajectory_by_function(elem, animate_func, direction, back) {

        // вся страница от -2*PI до 2*PI по ширинне или высоте (в зависимости от направления)

        var scale;  // масштаб
        var d_top;  // смещение относительно top
        var d_left; // смещение относительно left

        var x;
        var y;

        var top;    // конечное значение top
        var left;   // конечное значение left

        var X_STEP  = 0.05;     // шаг изменения x

        var to_back = false;    // обратный цикл?

        var stop_condition;         // возвращает true если достигли края экрана
        var stop_condition_back;    // возвращает true если достигли края экрана (при обратном цикле)

        function out_top()    { return top  <= 0; };
        function out_bottom() { return top  >= pv_3(); };
        function out_right()  { return left >= ph_3(); };
        function out_left()   { return left <= 0; };

        switch (direction) {
            // 0 - вверх  (↑)
            case 0: 
                scale  = ((v_height() - box_height) / 4) / Math.PI;
                d_top  = pv_3();
                d_left = ph_2();
                x = 0;
                y = -2 * Math.PI;
                stop_condition      = out_top;
                stop_condition_back = out_bottom;
                break;

            // 1 - вниз   (↓)
            case 1:
                scale  = ((v_height() - box_height) / 4) / Math.PI;
                d_top  = 0;
                d_left = ph_2();
                x = 0;
                y = 2 * Math.PI;
                stop_condition      = out_bottom;
                stop_condition_back = out_top;
                break;

            // 2 - вправо (→)
            case 2:
                scale  = ((v_width() - box_width) / 4) / Math.PI;
                d_top  = pv_2();
                d_left = ((v_width() - box_width) / 4) * 2;
                x = -2 * Math.PI;
                y = 0;
                stop_condition      = out_right;
                stop_condition_back = out_left;
                break;

            // 3 - влево  (←)
            case 3:
                scale  = ((v_width() - box_width) / 4) / Math.PI;
                d_top  = pv_2();
                d_left = ((v_width() - box_width) / 4) * 2;
                x = 2 * Math.PI;
                y = 0;
                stop_condition      = out_left;
                stop_condition_back = out_right;
                break;
        }

        iv = setInterval(function() {

            y = animate_func ? Math.cos(x) : Math.sin(x);

            switch (direction) {
                case 0:
                case 1:
                    left = y * scale + d_left;
                    top  = x * scale + d_top;
                    break;
                case 2:
                case 3:
                    left = x * scale + d_left;
                    top  = y * scale + d_top;
            }

            if (to_back) {
                (direction === 0 || direction === 3) ? x += X_STEP : x -= X_STEP;
            } else {
                (direction === 0 || direction === 3) ? x -= X_STEP : x += X_STEP;
            }

            if (!to_back && stop_condition()) {
                if (back) {
                    to_back = true;
                    (direction === 0 || direction === 3) ? x += X_STEP : x -= X_STEP; // возвращаем предыдущее значение x
                } else {
                    clearInterval(iv);
                    animate_trajectory_by_function(elem, animate_func, direction, back);
                }                
            } else
            if (to_back && stop_condition_back()) {
                clearInterval(iv);
                animate_trajectory_by_function(elem, animate_func, direction, back);
            } else {
                elem.css("top",  top);
                elem.css("left", left);
            }
        }, 17);
        
    }



    /* ============================================================ */
    /* управление при помощи экранных кнопок */
    /* ============================================================ */

    function top_min()  { return 0; }
    function top_max()  { return pv_3(); }
    function left_min() { return 0; }
    function left_max() { return ph_3(); }

    function move_element(elem, direction, count) {
        // direction 0 - ↑, 1 - →, 2 - ↓, 3 - ←
        var top;
        var left;
        
        if (direction === 0 || direction === 2)
            try { top  = parseInt(elem.css("top" )); } catch(e) { top  = 0; }
        else
            try { left = parseInt(elem.css("left")); } catch(e) { left = 0; }

        switch (direction) {
            case 0: if (top  - count >= top_min())  elem.css("top",  top  - count); break;
            case 1: if (left + count <= left_max()) elem.css("left", left + count); break;
            case 2: if (top  + count <= top_max())  elem.css("top",  top  + count); break;
            case 3: if (left - count >= left_min()) elem.css("left", left - count); break;
        }
    }

    $("#button-up"   ).mousedown(function() {
        iv = setInterval(function() { move_element(label, 0, 5); }, 17); return false;
    });
    $("#button-right").mousedown(function() {
        iv = setInterval(function() { move_element(label, 1, 5); }, 17); return false;
    });
    $("#button-down" ).mousedown(function() {
        iv = setInterval(function() { move_element(label, 2, 5); }, 17); return false;
    });
    $("#button-left" ).mousedown(function() {
        iv = setInterval(function() { move_element(label, 3, 5); }, 17); return false;
    });

    $("#button-up"   ).mouseup( function() { clearInterval(iv); return false; });
    $("#button-right").mouseup( function() { clearInterval(iv); return false; });
    $("#button-down" ).mouseup( function() { clearInterval(iv); return false; });
    $("#button-left" ).mouseup( function() { clearInterval(iv); return false; });



    /* ============================================================ */
    /* управление при помощи клавиатуры */
    /* ============================================================ */

    $(document).keydown(function(e) {
        switch(e.which) {
            // left
            case 37: move_element(label, 3, 5); break;
            // up
            case 38: move_element(label, 0, 5); break;
            // right
            case 39: move_element(label, 1, 5); break;
            // down
            case 40: move_element(label, 2, 5); break;

            default: return; // exit this handler for other keys
        }
        e.preventDefault(); // prevent the default action (scroll / move caret)
    });



    /* ============================================================ */
    /* элемент можно перетаскивать мышью */
    /* ============================================================ */

    /* jQuery UI, Draggable */



    /* ============================================================ */
    /* элемент преследует курсор мыши */
    /* ============================================================ */

    $(document).on("mousemove", function(e) {

        var x = e.pageX + 1;
        var y = e.pageY + 1;

        if (follow_cursor && y >= top_min() && y <= top_max() && x >= left_min() && x <= left_max())
            label.css({ top: y, left: x });

    });



    /* ============================================================ */
    /* элемент убегает от курсора мыши */
    /* ============================================================ */

    function random_integer(min, max) {
        var rand = min + Math.random() * (max - min)
        rand = Math.round(rand);
        return rand;
    }

    label.on("mouseover", function(e) {

        var x;
        var y;

        if (cursor_runaway) {
            x = random_integer(left_min(), left_max());
            y = random_integer(top_min(),  top_max());    

            label.css({ top: y, left: x });
        }
    });

});